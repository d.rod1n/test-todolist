package handlers

import (
	"context"
	"encoding/json"
	"net/http"
)

type Handlers struct{}

type StoragePing interface {
	Ping(ctx context.Context) error
}

type response struct {
	Answer string `json:"answer"`
}

func (h *Handlers) Ping(res http.ResponseWriter, req *http.Request, db StoragePing) {
	ctx := req.Context()
	if err := db.Ping(ctx); err != nil {
		resp := response{Answer: "db not connected"}
		ans, err := json.Marshal(resp)
		if err != nil {
			res.WriteHeader(http.StatusBadRequest)
			return
		}
		res.WriteHeader(http.StatusInternalServerError)
		res.Write(ans)
		return
	}
	resp := response{Answer: "db connected"}
	ans, err := json.Marshal(resp)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	res.WriteHeader(http.StatusOK)
	res.Write(ans)
}
