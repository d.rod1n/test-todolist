package config

import "os"

type Config struct {
	MongoURI string
}

func NewConfig() *Config {
	mongoURI := os.Getenv("MONGO_URL")
	return &Config{
		MongoURI: mongoURI,
	}
}
