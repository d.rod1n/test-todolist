package api

import (
	"context"
	"test-todolist/internal/handlers"

	"github.com/gin-gonic/gin"
)

type Storage interface {
	Ping(ctx context.Context) error
}

const (
	groupAPI  = "/api"
	groupTODO = "/todo-list"
	routePing = "/ping"
)

func NewAPI(db Storage) (*gin.Engine, error) {
	r := gin.Default()
	h := &handlers.Handlers{}
	r.GET(routePing, func(c *gin.Context) {
		w := c.Writer
		r := c.Request
		h.Ping(w, r, db)

	})

	return r, nil
}
