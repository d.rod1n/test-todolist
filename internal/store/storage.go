package store

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Store struct {
	client *mongo.Client
}

func NewStore(uri string) (*Store, error) {
	serverAPI := options.ServerAPI(options.ServerAPIVersion1)

	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(uri).SetServerAPIOptions(serverAPI))
	if err != nil {
		return nil, fmt.Errorf("failed to connect mongodb: %w", err)
	}

	return &Store{
		client: client,
	}, nil
}

func (st *Store) Ping(ctx context.Context) error {
	if err := st.client.Ping(ctx, nil); err != nil {
		return fmt.Errorf("failed to ping storage: %w", err)
	}
	return nil
}
