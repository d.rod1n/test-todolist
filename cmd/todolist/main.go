package main

import (
	"context"
	"fmt"
	"log"
	"test-todolist/internal/api"
	"test-todolist/internal/config"
	"test-todolist/internal/store"
)

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}

func run() error {
	cfg := config.NewConfig()

	db, err := store.NewStore(cfg.MongoURI)
	if err != nil {
		return fmt.Errorf("failed to initialize storage: %w", err)
	}

	ctx := context.TODO()
	if err := db.Ping(ctx); err != nil {
		return fmt.Errorf("failed to Ping storage: %w", err)
	}

	api, err := api.NewAPI(db)
	if err != nil {
		return fmt.Errorf("failed to initialize api: %w", err)
	}

	if err := api.Run(":5000"); err != nil {
		return fmt.Errorf("failed to start api: %w", err)
	}
	return nil
}
